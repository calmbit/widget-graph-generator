angular.module("risevision.common.i18n.config", [])
	    .constant("LOCALES_PREFIX", "../../bower_components/rv-common-i18n/dist/locales/translation_")
	    .constant("LOCALES_SUFIX", ".json");

angular.module("lcdi.widget.graphGenerator.settings", [
  "risevision.common.i18n",
  "risevision.widget.common",
  "risevision.widget.common.widget-button-toolbar",
	'color.picker'
]);
