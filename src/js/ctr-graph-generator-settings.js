angular.module("lcdi.widget.graphGenerator.settings").value("defaultSettings", {
	params: {chartType: "1",
						chartSourcing: "1",
						chartBackgroundColour: "hsl(34.6,100%,89.8%)",
						chartTitleColour: "hsl(225.1,100%,70%)"},
	additionalParams: {}
});
