function getPercent(data) {
  var total = 0;
  for(var i = 0;i < sampleData.length;i++)
  {
    total += sampleData[i];
  }
  return ((data / total) * 100).toFixed(1);
}

function readyEvent() {
  gadgets.rpc.call("",
                  "rsevent_ready",
                   null,
                   prefs.getString("id"),
                   false,
                   false,
                   false,
                   true,
                   false);
}

const GRAPH_TYPE_BAR = 1;
const GRAPH_TYPE_PYRAMID = 2;
const GRAPH_TYPE_PIE = 3;
const GRAPH_TYPE_OTHER = 4;

// Testing variable, will be bound to an angular variable at runtime.
var graphType = GRAPH_TYPE_PYRAMID;

//===================== SUPER IMPORTANT ===========================\\
// This data is purely for testing - please TODO: Replace this.    \\
var sampleData = [20, 19, 17, 15].sort()
var sampleLabels = ["Second Years",
                      "First Years",
                      "Third Years",
                      "Fourth Years"];

// Here's some magic constants we can divine from testing...
var scale = 4;
var midpoint = 500;
var fontSizeLabel = 18;
var fontSizePercent = 20;

var fills = ["#1F1FFF","#FFFF1F"];
var labelColours = ["#FFFFFF","#1F1FFF"];
var graphBackground = "hsl(34.6,100%,89.8%)";

var graphTitle = "LCDI Workforce Statistics";

var graphTitleColour = "hsl(225.1,100%,70%)";
var graphTitleSize = 72;

var graphOffsetX = 0;
var graphOffsetY = graphTitleSize;

switch(graphType) {
  case GRAPH_TYPE_PYRAMID:
    drawPyramidGraph()
    break;
  default:
    break;
}

d3.select("svg")
  .style("background-color", graphBackground);

d3.select("svg").append("text")
    .text(graphTitle)
    .style("font-size", graphTitleSize + "px")
    .style("font-variant", "small-caps")
    .style("font-family", "sans-serif")
    .style("font-weight", "bold")
    .attr("fill", graphTitleColour)
    .attr("y", graphTitleSize)
    .attr("x", function() {
      return midpoint-((graphTitle.length*graphTitleSize)/4.25)
    });

function drawPyramidGraph() {
  var rise = 50*scale;
  var run = 25*scale;

  var group = d3.select(".test-chart")
    .selectAll("svg")
      .data(sampleData)
    .enter().append("g")
      .attr("transform", function(d, i) {
        return "translate(0," + (i+1)*30 + ")";
      });

  group.append("polygon")
    .attr("points", function(d, i) {
      var y = i*42.5*scale;
      // Is it a triangle?
      if(i == 0)
      {
        return (midpoint)+","+(y+graphOffsetY) +" "+
               (midpoint-run)+","+(y+rise+graphOffsetY) +" "+
               (midpoint+run)+","+(y+rise+graphOffsetY);
      }
      // Alright, so it's a quad.
      else
      {
        i = i+1;
         return (midpoint-run*(i-1))+","+(y+graphOffsetY) +" "+
                (midpoint-run*i)+","+(y+rise+graphOffsetY) +" "+
                (midpoint+run*i)+","+(y+rise+graphOffsetY) +" "+
                (midpoint+run*(i-1))+","+(y+graphOffsetY);
      }
    })
    // Alternate between two fills...
    .attr("fill", function(d,i) {
      return fills[i % fills.length];
    })
    .attr("stroke", "#000000")
    .classed("graph-bar", true);

  group.append("text")
    .text(function(d, i) {
      return sampleLabels[i];
    })
    .attr("y", function(d, i) {
      return i*40*scale+(rise/1.5)+graphOffsetY;
    })
    .attr("x", function(d, i) {
      // We add the +1 because of the boldness of the text.
      var scalingFontSize = (fontSizeLabel+((i*fontSizeLabel)/2)) + 1;
      var textLengthQuotient = (sampleLabels[i].length/4);
      return midpoint-(scalingFontSize*textLengthQuotient);
    })
    .style("font-size", function(d,i) {
      var scalingFontSize = (fontSizeLabel+((i*fontSizeLabel)/2));
      return scalingFontSize + "px";
    })
    .style("font-family", "sans-serif")
    .style("font-weight", "bold")
    .attr("id",function(d,i){return "label-"+i})
    .attr("fill", function(d,i) {
      return labelColours[i % labelColours.length];
    });

  group.append("text")
    .text(function(d, i) {
      return getPercent(d) + "%";
    })
    .attr("y", function(d, i) {
      return i*40*scale+(rise/1.115)+graphOffsetY;
    })
    .attr("x", function(d, i) {
      var scalingFontSize = (fontSizePercent+((i*fontSizePercent)/2)) + 1;
      var textLengthQuotient = (sampleLabels[i].length/8);
      return midpoint-(scalingFontSize*textLengthQuotient);
    })
    .style("font-size", function(d,i) {
      var scalingFontSize = (fontSizePercent+((i*fontSizePercent)/2));
      return scalingFontSize + "px"
    })
    .style("font-family", "sans-serif")
    .style("font-weight", "bold")
    .attr("fill", function(d,i) {
      return labelColours[i % labelColours.length];
    });
}


readyEvent();
